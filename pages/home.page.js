class HomePage {
    get searchBar() { return $('#search_query_top'); }
    get searchButton() { return $('.button-search'); }

    searchFor(searchTerm) {
        this.searchBar.clearElement();
        this.searchBar.setValue(searchTerm);
        this.searchButton.click();
        WaitUntil.pageTitleEquals('Search - My Store'); 
    };
};

module.exports = new HomePage;