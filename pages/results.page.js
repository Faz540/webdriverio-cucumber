class ResultsPage {
    get numberOfItems() { return parseInt(browser.getText('.heading-counter')[0]); }
};

module.exports = new ResultsPage;