class ProductPage {
    get addToCart() { return $('//*[@id="add_to_cart"]/button'); }
    get cartPopup() { return $('#layer_cart'); }

    addItemToCart() {
        this.addToCart.click();
        WaitUntil.elementIsVisibleOnPage(this.cartPopup);
    }
};

module.exports = new ProductPage;