Feature: Product Page tests

Background: 
    Given I am viewing the Product "?id_product=1&controller=product"

Scenario: Users are able to add the item to their cart
    When I click the Add To Cart button
    Then The message "Product successfully added to your shopping cart" should be displayed