Feature: Home Page tests

Background: 
    Given I am on the Home Page

Scenario: The expected page title exists on the Home Page
    Then The page title should equal "My Store";

Scenario: Users are able to perform a search and get results
    When I search for "black dresses"
    Then I expect at least one item to be returned

Scenario: The above test can run using the Page Object model
    When I search for "black dresses" using Page Objects
    Then I expect at least one item to be returned using Page Objects