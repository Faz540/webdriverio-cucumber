const expect = require('chai').expect;
const {defineSupportCode} = require('cucumber');

defineSupportCode(function( {Given, When, Then} ) {
    Given('I am on the Home Page', function() {
        browser.url('./');
    });
    When('I search for "{string}"', function(text) {
        browser.element('#search_query_top').clearElement();
        browser.element('#search_query_top').setValue('black dresses');
        browser.element('.button-search').click();
        browser.waitUntil(function () { return browser.getTitle() === 'Search - My Store' });
    });
    Then('I expect at least one item to be returned', function() {
        let numberOfItems = browser.getText('.heading-counter')[0];
        numberOfItems = parseInt(numberOfItems);
        expect(numberOfItems).to.be.greaterThan(0);
    });
    Then('The page title should equal "{string}', function(text) {
        const title = browser.getTitle();
        expect(title).to.equal('My Store');
    });
    When('I search for "{string}" using Page Objects', function(text) {
        HomePage.searchFor(text);
    });
    Then('I expect at least one item to be returned using Page Objects', function() {
        expect(ResultsPage.numberOfItems).to.be.greaterThan(0);
    });
});

