const expect = require('chai').expect;
const {defineSupportCode} = require('cucumber');

defineSupportCode(function( {Given, When, Then} ) {
    Given('I am viewing the Product "{string}"', function(text) {
        browser.url(`./${text}`);
    });
    When('I click the Add To Cart button', function() {
        ProductPage.addItemToCart();
    });
    Then('The message "{string}" should be displayed', function(text) {
        const cartPopupText = ProductPage.cartPopup.getText();
        expect(cartPopupText).to.contain('Product successfully added to your shopping cart')
    });
});