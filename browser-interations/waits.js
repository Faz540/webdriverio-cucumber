class WaitUntil {
    pageTitleEquals(expectedPageTitle) {
        browser.waitUntil(function () {
            return browser.getTitle() === expectedPageTitle
        }, 10000, `expected page title to equal ${expectedPageTitle}`);
    }

    elementIsVisibleOnPage(element) {
        browser.waitForVisible(element.selector);
    }
}

module.exports = new WaitUntil;